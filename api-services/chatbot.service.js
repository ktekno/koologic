"use strict"

const express = require("express");
const fetch = require('node-fetch');
const { wooApi } = require("../config/config"); 

const chatbotApi = express.Router();

chatbotApi.get("/list-products-bot/:category", async function(req, res){
    try{
        let productTags = (await wooApi.get("products/tags")).data;
        let productTagIdIndex = productTags.findIndex(r => r.slug == req.params.category);
        let productTagId = productTags[productTagIdIndex].id;
        let description = productTags[productTagIdIndex].description;
        let productInfo = (await wooApi.get("products?tag="+productTagId)).data;
        let buttons = [];
        let messageFormat = {
            messages: [
                {
                    attachment: {
                        type: "template",
                        payload: {
                                    template_type: "generic",
                                    image_aspect_ratio: "square",
                                    elements: []
                        }
                    }
                }
            ]
        }
        productInfo.forEach(function(product){
            product.category_name = description;
            buttons = [];
            if(product.variations.length > 0){
                product.variations.forEach(function(variation, index){
                    if(buttons.length < 3) {
                        buttons.push({
                            type:"show_block",
                            block_names: ["Add to cart"],
                            set_attributes: {"product_id": variation},
                            title:"Add " + product.attributes[0].options[index] +  " to cart"
                        });
                    }
                });
            } else {
                buttons.push({
                    type:"show_block",
                    block_names: ["Add to cart"],
                    set_attributes: {"product_id": product.id},
                    title:"Add to cart"
                });
            }
            messageFormat.messages[0].attachment.payload.elements.push({
                title: product.name,
                subtitle: "Price: PHP" + parseFloat(product.sale_price != ""? parseFloat(product.sale_price) : parseFloat(product.price)) + "\n Click the image for product specs",
                default_action: {
                    type: "web_url",
                    url: "https://koologic-ph.com/product/" + product.slug
                },
                image_url: "https://www.koologic-ph.com/wpbase/wp-content/uploads/2021/04/KB8.jpg",
                buttons: buttons
            });
        });
        res.status(200).send(messageFormat);
    } catch (e){
        console.log(e);
        res.status(400).send("Product not found")
    }
});

chatbotApi.post("/cart-bot/update", async function(req, res){
    let cart_items = req.body.cart_items && req.body.cart_items != "null" && typeof req.body.cart_items !== "undefined"? JSON.parse(req.body.cart_items) :[];
    let productInfo = await wooApi.get("products/" + req.body.product_id);
    let product_id_index = cart_items.findIndex(r => r.product_id == req.body.product_id);
    if(product_id_index > -1){
        cart_items[product_id_index].quantity = parseFloat(cart_items[product_id_index].quantity);
        cart_items[product_id_index].quantity += parseFloat(req.body.quantity);
        cart_items[product_id_index].price += parseFloat(productInfo.data.sale_price != ""? productInfo.data.sale_price: productInfo.data.price) * parseFloat(cart_items[product_id_index].quantity);
    } else {
        cart_items.push({
            name: productInfo.data.name,
            slug: productInfo.data.slug,
            product_id: req.body.product_id,
            quantity: req.body.quantity,
            price:  parseFloat(productInfo.data.sale_price != ""? productInfo.data.sale_price: productInfo.data.price) * parseFloat(req.body.quantity),
            weight: parseFloat(productInfo.data.weight) * parseFloat(req.body.quantity),
            image_url: productInfo.images[0].src,
        })

    }
    
    let messageFormat = {
        set_attributes: {
            "cart_items": JSON.stringify(cart_items),
        },
        messages: [ ]
    }
    res.status(200).send(messageFormat);
});

chatbotApi.post("/cart-bot/delete", async function(req, res){
    let cart_items = req.body.cart_items && req.body.cart_items != "null" && typeof req.body.cart_items !== "undefined"? JSON.parse(req.body.cart_items) :[];
    let product_id_index = cart_items.findIndex(r => r.product_id == req.body.product_id);
    if(product_id_index > -1){
        cart_items.splice(product_id_index, 1);
    }    
    let messageFormat = {
        set_attributes: {
            "cart_items": JSON.stringify(cart_items),
        },
        messages: [ ]
    }
    res.status(200).send(messageFormat);
});

chatbotApi.post("/cart-bot/status", async function(req, res){
    let cart_items = req.body.cart_items && req.body.cart_items != "null" && typeof req.body.cart_items !== "undefined"? JSON.parse(req.body.cart_items) :[];
    let messageFormat = {
        messages: [
            {
                attachment: {
                    type: "template",
                    payload: {
                                template_type: "generic",
                                image_aspect_ratio: "square",
                                elements: []
                    }
                }
            }
        ]
    }
    cart_items.forEach(function(item){
        messageFormat.messages[0].attachment.payload.elements.push({
            title: item.name,
            subtitle: "Qty: "+ item.quantity + ", Price: PHP " + item.price,
            default_action: {
                type: "web_url",
                url: "https://koologic-ph.com/product/" + item.slug
            },
            image_url: "https://www.koologic-ph.com/wpbase/wp-content/uploads/2021/04/KB8.jpg",
            buttons: [{
                    type:"show_block",
                    block_names: ["Remove from cart"],
                    set_attributes: {"product_id": item.product_id},
                    title:"Remove from cart"
            }, {
                type:"show_block",
                block_names: ["Checkout"],
                title:"Checkout"
            }]
        });
    });
    if(cart_items.length == 0) {
        messageFormat = {
            messages: [
              {
                attachment: {
                    type: "template",
                    payload: {
                    template_type: "button",
                    text: "Your cart is empty",
                    buttons: [
                                {
                                type: "show_block",
                                block_names: ["View Products"],
                                title: "View Products"
                                }
                            ]
                        }
                    }
                }
            ]
        }
    }
    res.status(200).send(messageFormat);
});

chatbotApi.post("/cart-bot/checkout", async function(req, res){
    let cart_items = req.body.cart_items && req.body.cart_items != "null" && typeof req.body.cart_items !== "undefined"? JSON.parse(req.body.cart_items) :[];
    let billing_message = "";
    let buttons = [];
    if(cart_items.length == 0){
        billing_message = "You cart is empty";
        buttons = [
            {
              type: "show_block",
              block_names: [
                "View Products"
              ],
              title: "View Products"
            }
        ]
    } else {
        let total_price = 0;
        billing_message = "You cart contains:";
        cart_items.forEach(function(item){
            billing_message += "\n" + item.name + "(" + item.quantity + ", PHP"+ item.price + ")";
            total_price += parseFloat(item.price);
        });
        billing_message += "\n\nTotal Price: PHP" + total_price;
        buttons = [
            {
              type: "show_block",
              block_names: [
                "View Products"
              ],
              title: "Continue shopping"
            },
            {
              type: "show_block",
              block_names: [
                "Payment Methods"
              ],
              title: "Proceed to payment"
            }
        ]

    }
    let messageFormat = {
        messages: [
            {
            attachment: {
                type: "template",
                payload: {
                        template_type: "button",
                        text: billing_message,
                        buttons: buttons
                    }
                }
            }
        ]
    }
    res.status(200).send(messageFormat);
});

chatbotApi.post("/cart-bot/validate-payment", async function(req, res){
    let ref_no = req.body.ref_no;
    let payment_type = req.body.payment_type;
    if (ref_no.match(/[0-9]{9,14}/gi) == null && payment_type == "gcash"){
        res.status(200).send({
            redirect_to_blocks: ["Error GCash"]
        });
    }
    else {
        res.status(200).send({
            redirect_to_blocks: ["Receipt"]
        });
    }
});


chatbotApi.post("/cart-bot/create-order", async function(req, res){
    let order_id = -1;
    let delivery_id = -1;
    try{
        let cart_items = req.body.cart_items && req.body.cart_items != "null" && typeof req.body.cart_items !== "undefined"? JSON.parse(req.body.cart_items) :[];
        let delivery_info = {};
        let order_data = {};
        let total_weight_kg = 0;
        let [street_1, street_2, city, state, postal_code] = req.body.address.split(",");
        let messageFormat = {
            messages: [
                {
                    attachment: {
                        type: "template",
                        payload: {
                            template_type: "receipt",
                            recipient_name: req.body.name,
                            order_number: "12345678901",
                            currency: "PHP",
                            payment_method: req.body.payment_method,
                            order_url: "https://koologic-ph.com",
                            timestamp: Math.round(new Date().getTime()/1000),
                            address: {
                              street_1: street_1.trim(),
                              street_2: street_2.trim(),
                              city: city.trim(),
                              postal_code: postal_code.trim(),
                              state: state.trim(),
                              country: "PH"
                            },
                            summary: {
                            },
                            adjustments: [],
                            elements: []
                        }
                    }
                }
            ]
        }
        req.body.billing = {};
        req.body.shipping = {};
        req.body.billing.first_name = req.body.name.substring(0, req.body.name.indexOf(' '));
        req.body.billing.last_name = req.body.name.substring(req.body.name.indexOf(' ') + 1);
        req.body.billing.phone = req.body.phone;
        req.body.billing.address_1 = street_1;
        req.body.billing.address_2 = street_2;
        req.body.billing.city = city;
        req.body.billing.state = state;
        req.body.billing.postcode = postal_code;

        req.body.shipping.first_name = req.body.name.substring(0, req.body.name.indexOf(' '));
        req.body.shipping.last_name = req.body.name.substring(req.body.name.indexOf(' ') + 1);
        req.body.shipping.phone = req.body.phone;
        req.body.shipping.address_1 = street_1;
        req.body.shipping.address_2 = street_2;
        req.body.shipping.city = city;
        req.body.shipping.state = state;
        req.body.shipping.postcode = postal_code;
        req.body.line_items = [];

        if(cart_items.length > 0){
            cart_items.forEach(function(cart_item){
                req.body.line_items.push({
                    product_id: cart_item.product_id,
                    quantity: cart_item.quantity
                });
                total_weight_kg += cart_item.weight;
            });
        } else {
            let billing_message = "You cart is empty";
            let buttons = [
                {
                  type: "show_block",
                  block_names: [
                    "View Products"
                  ],
                  title: "View Products"
                }
            ];
            messageFormat = {
                messages: [
                    {
                    attachment: {
                        type: "template",
                        payload: {
                                template_type: "button",
                                text: billing_message,
                                buttons: buttons
                            }
                        }
                    }
                ]
            }
            res.status(200).send(messageFormat);
        }
        
        order_data = (await wooApi.post("orders", req.body)).data
        order_id = order_data.id;
        delivery_info = await (await fetch(JSON.parse(process.env.MR_SPEEDY).url + 'create-order', {
            method: 'POST',
            body:    JSON.stringify({
                matter:"Gadgets",
                is_client_notification_enabled: true,
                is_route_optimizer_enabled: true,
                vehicle_type_id: 8,
                total_weight_kg: total_weight_kg,
                points:[{
                    address:"70 Jasmine, St. Lodora Village, Brgy Tunasan, Muntinlupa, NCR",
                    contact_person: { 
                        name: "Kydo Solis",
                        phone: "09054302834"
                    },
                    client_order_id: order_id
                },{
                    address: street_1.trim() + street_2.trim() + ", " + city.trim() + ", " + state.trim(),
                    contact_person: { 
                        name: req.body.shipping.first_name + " " + req.body.shipping.last_name,
                        phone: req.body.shipping.phone
                    },
                    client_order_id: order_id
                }]
            }),
            headers: { 
                'Content-Type': 'application/json',
                'X-DV-Auth-Token': JSON.parse(process.env.MR_SPEEDY).api_key,
            },
            credentials: "include"
        })).json();
        delivery_id = delivery_info.order.order_id;
        order_data = (await wooApi.put("orders/" + order_id, {
            shipping_lines:[{
                method_id: "shipping2",
                method_name: "Mr Speedy",
                total: delivery_info.order.delivery_fee_amount
            }],
            meta_data: [{
                key: "shipment_tracking",
                value:  delivery_info.order.order_id
            }]
        })).data;
        order_id = order_data.id;
        messageFormat.messages[0].attachment.payload.summary = {
            subtotal: parseFloat(order_data.total) - parseFloat(order_data.shipping_total),
            shipping_cost: parseFloat(order_data.shipping_total),
            total_tax: 0,
            total_cost: parseFloat(order_data.total)
        };
        messageFormat.messages[0].attachment.payload.order_number = order_id + "";
        order_data.line_items.forEach(function(item){
            let image_index = cart_items.findIndex(r => parseInt(r.product_id) == (item.variation_id != 0? item.variation_id : item.product_id));
            messageFormat.messages[0].attachment.payload.elements.push({
                title: item.parent_name != ""? item.parent_name : item.name,
                subtitle: item.parent_name != ""? item.name.replace(item.parent_name,'').trim() : '',
                quantity: parseInt(item.quantity),
                price: parseFloat(item.total),
                currency: "PHP",
                image_url: cart_items[image_index].image_url
            });
        });
        res.status(200).send(messageFormat);
    } catch (e){
        console.log(e);
        if(delivery_id != -1){
            await (await fetch(JSON.parse(process.env.MR_SPEEDY).url + 'cancel-order', {
                method: 'POST',
                body:    JSON.stringify({"order_id":delivery_id}),
                headers: { 
                    'Content-Type': 'application/json',
                    'X-DV-Auth-Token': JSON.parse(process.env.MR_SPEEDY).api_key,
                },
                credentials: "include"
            })).json();
        }
        if(order_id != -1){
            (await wooApi.delete("orders/"+order_id, { force: true }));
        }
        
        res.status(200).send({
            redirect_to_blocks: ["Error User"]
        });
    }
});

module.exports = chatbotApi;